# Wiremock Beispiele

![Wiremock-Logo](./__files/wiremock.png)

Mit Hilfe des Wiremock-Severs kann man folgendes tun:
* Stubbing - Request-Mapping mit definierten Responses
* Verifying -  Prüfung der Aufrufe
* Request Matching - Prüfen/Filtern auf spezielle Attribute
* Response Templating - Dynamische Antworten
* Simulating Faults - Fehlerszenarios - Edge Cases
* Stateful Behaviour with Scenarios - Komplexe Abläufe - Implementiertes Test Double


## Starten des Wiremock Standalone Servers
```
java -jar wiremock-standalone-2.27.2.jar
```
Ausgabe:
```
SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
 /$$      /$$ /$$                     /$$      /$$                     /$$
| $$  /$ | $$|__/                    | $$$    /$$$                    | $$
| $$ /$$$| $$ /$$  /$$$$$$   /$$$$$$ | $$$$  /$$$$  /$$$$$$   /$$$$$$$| $$   /$$
| $$/$$ $$ $$| $$ /$$__  $$ /$$__  $$| $$ $$/$$ $$ /$$__  $$ /$$_____/| $$  /$$/
| $$$$_  $$$$| $$| $$  \__/| $$$$$$$$| $$  $$$| $$| $$  \ $$| $$      | $$$$$$/
| $$$/ \  $$$| $$| $$      | $$_____/| $$\  $ | $$| $$  | $$| $$      | $$_  $$
| $$/   \  $$| $$| $$      |  $$$$$$$| $$ \/  | $$|  $$$$$$/|  $$$$$$$| $$ \  $$
|__/     \__/|__/|__/       \_______/|__/     |__/ \______/  \_______/|__/  \__/

port:                         8080
enable-browser-proxying:      false
disable-banner:               false
no-request-journal:           false
verbose:                      false
```

[Informationen zu den Console-Prametern](http://wiremock.org/docs/running-standalone/)

### Urls
#### Basis-URL  
http://localhost:8080


#### Admin-URL
Mit dieser URL kann man die gespeicherten Mappings sehen.

http://localhost:8080/__admin/

### Mock programmieren
Um einen stub für eine JSON-API zu programmieren, sendet man einen POST-Request an http://localhost:8080/__admin/mappings mit folgendem Beispiel-Body:
```
{
    "request": {
        "method": "GET",
        "url": "/some/thing"
    },
    "response": {
        "status": 200,
        "body": "Hello world!",
        "headers": {
            "Content-Type": "text/plain"
        }
    }
}
```

Alternativen:
 * Man legt eine Datei mit der Dateiendung __.json__ mit entsprechendem Inhalt in das Verzeichnis __mappings__
 * Curl POST-Request:
    ```
   curl --location --request POST 'http://localhost:8080/__admin/mappings' \
   --header 'Content-Type: text/plain' \
   --data-raw '{
       "request": {
           "method": "GET",
           "url": "/some/thing"
       },
       "response": {
           "status": 200,
           "body": "Hello world!",
           "headers": {
               "Content-Type": "text/plain"
           }
       }
   }'
    ```
   * Via Postman
   
    ![POST-Request via Postman](./docs/images/postman-add.jpg)
 
### Mock programmieren durch aufzeichnen
```
java -jar wiremock-standalone-2.27.2.jar --proxy-all [URL] --record-mappings
```
## Server starten
Beispiel:
```
java -jar wiremock-standalone-2.27.2.jar --proxy-all https://fakeapi.andreaspabst.com/api/ --record-mappings


SLF4J: Failed to load class "org.slf4j.impl.StaticLoggerBinder".
SLF4J: Defaulting to no-operation (NOP) logger implementation
SLF4J: See http://www.slf4j.org/codes.html#StaticLoggerBinder for further details.
 /$$      /$$ /$$                     /$$      /$$                     /$$
| $$  /$ | $$|__/                    | $$$    /$$$                    | $$
| $$ /$$$| $$ /$$  /$$$$$$   /$$$$$$ | $$$$  /$$$$  /$$$$$$   /$$$$$$$| $$   /$$
| $$/$$ $$ $$| $$ /$$__  $$ /$$__  $$| $$ $$/$$ $$ /$$__  $$ /$$_____/| $$  /$$/
| $$$$_  $$$$| $$| $$  \__/| $$$$$$$$| $$  $$$| $$| $$  \ $$| $$      | $$$$$$/
| $$$/ \  $$$| $$| $$      | $$_____/| $$\  $ | $$| $$  | $$| $$      | $$_  $$
| $$/   \  $$| $$| $$      |  $$$$$$$| $$ \/  | $$|  $$$$$$/|  $$$$$$$| $$ \  $$
|__/     \__/|__/|__/       \_______/|__/     |__/ \______/  \_______/|__/  \__/

port:                         8080
proxy-all:                    https://fakeapi.andreaspabst.com/api/
preserve-host-header:         false
enable-browser-proxying:      false
disable-banner:               false
record-mappings:              true
match-headers:                []
no-request-journal:           false
verbose:                      false
```

## Aufzeichnen
Beispiel:

Durch einen GET-Request, ausgelöst durch den Browser, wird der Request durch den Mock-Server an den zu mockenden Service geleitet und dessen Response aufgezeichnet.

```
http://localhost:8080/users
```
![POST-Request via Postman](./docs/images/browser-get.jpg)

Die persistierten Dateien liegen in den Verzeichnissen:
```
.
|-- __files  <-- hier liegt der gespeicherte Response-Body
|   |-- body-users-Kned1.json
|-- mappings <-- hier liegt das Request / Response - Mapping
|   |-- mapping-users-Kned1.json
|
|-- wiremock-standalone-2.27.2.jar
```

## Default für nicht gemappte Requests programmieren

POST-Request an http://localhost:8080/__admin/mappings mit folgendem Beispiel-Body:
```
{
  "priority":10,
  "request": {
    "method": "ANY",
    "urlPattern": ".*"
  },
  "response": {
    "status": 404,
    "jsonBody": {"status":"Error","message":"Endpoint not found"},
    "headers": {
      "Content-Type": "application/json"
    }
  }
}
```

## Dynamisch erzeugte Mappings persistieren

POST-Request an http://localhost:8080/__admin/mappings/save

## Statische Dateien 'ausliefern'

Alle Dateien im Verzeichnis __files werden über einen GET-Request geliefert.

### Beispiel

Das Wiremock-Logo __files/wiremock.png kann über http://localhost:8080/wiremock.png abgerufen werden.

Das funktioniert nur dann wenn keine default-mapping Regel hinterlegt wurde. Es wird immer erst geprüft ob eine Regel anzuwenden ist, erst dann wird in __files gesucht. 

## Löschen eines Mappings

Mit einem DELETE Request an die URL http://localhost:8080/__admin/mappings/{UUID}, wobei die UUID durch die ID des gemappten Requests ersetzt wird.

### Beispiel Löschen der default-Mapping Regel

DELETE Request an http://localhost:8080/__admin/mappings/25137f35-9ca4-48db-a0a8-dfed1f61fa90

### Verifying
Der WireMock-Server zeichnet alle ausgeführten Request auf (zumindest bis Neustart oder einem Reset). Mit diesem Feature kann überprüft werden ob gewünschte Request abgefordert wurden. z.B. wurde /some/thing fünf mal aufgerufen etc.
Die Wiremock-Abfragen werden in einem Journal protokolliert, für Last-Test, sollt das Journal deaktiviert werden. (siehe Konfiguration in der offiziellen Wiremock-Doku)

### Request Matching
Wiremock unterstützt eine Vielzahl von Attributen die das Matching (filtern) von Stubs bzw. Verificatin query genutzt werden können. 

   * URL
   * HTTP Method
   * Query parameters
   * Headers
   * Basic authentication (a special case of header matching)
   * Cookies
   * Request body
   * Multipart/form-data

### Response Templating
Mit diesem Feature kann der Response dynamisch durch Platzhaler angespasst werden.

###  Simulating Faults
Mit diesem Feature lassen sich Fehlerszenarien simulieren.
Beispiele:
    * Antwortverzögerung, je Request oder global
    * zufällige Antwortverzögerung, je Request oder global
    * böse Responses, z.B. Connection reseted by peer oder halber Response usw.

### Stateful Behaviour with Scenarios

Aufwändigere Szenarien können mit Szenarios abgebildet werden. Siehe http://wiremock.org/docs/stateful-behaviour/

-------

### Nützliche Skripte
 * Leeren der gespeicherten Mappings
    ```
    scripts/cleanup.sh
    ```
 * Beipieldaten hinterlegen
    ```
    scripts/demodata.sh
    ```
 
### Tools
 * [Postman](https://www.postman.com/)
 
### Weitere Links
 * [Wiremock](https://learning.postman.com/docs/getting-started/introduction/)
    * [Wiremock stubbing - Mappings programmieren](http://wiremock.org/docs/stubbing/)
    * [Youtube-Video Wiremock: because your microservice needs a buddy when you're testing by Rosanne Joosten](https://www.youtube.com/watch?v=ddUMyLsDkLw)
 * [Postman getting started](https://learning.postman.com/docs/getting-started/introduction/)
 * [Eine Fake-API für Testcalls](https://fakeapi.andreaspabst.com/)
 * [API Call, liefert JSON mit Liste von Usern](https://fakeapi.andreaspabst.com/api/users)
